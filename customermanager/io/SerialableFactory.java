package io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import model.Customer;

public class SerialableFactory {
	public static boolean saveFile(ArrayList<Customer>dsKH,String path) {
		try {
			FileOutputStream fos = new FileOutputStream(path);
			ObjectOutputStream oos= new ObjectOutputStream(fos);
			oos.writeObject(dsKH);
			fos.close();
			return true;
		} catch (Exception e) {
			//alert errors detail
			e.printStackTrace();
		}
		return false;		
	}
	
	@SuppressWarnings("unchecked")
	public static ArrayList<Customer>readfile (String path) {
		ArrayList<Customer>dsKH = new ArrayList<Customer>();
		try {
			FileInputStream fis = new FileInputStream(path);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object data = ois.readObject();
			dsKH = (ArrayList<Customer>) data;
			ois.close();
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dsKH;
		
	}
}
