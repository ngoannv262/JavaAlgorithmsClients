package model;

import java.io.Serializable;


public class Customer implements Serializable, Comparable<Customer>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	
	private int id ;
	private String phone;
	private String name;
	
	public Customer() {
		super();
	}

	public Customer(int id, String phone, String name) {
		super();
		this.id = id;
		this.phone = phone;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTen() {
		return name;
	}

	public void setTen(String ten) {
		this.name = ten;
	}

	@Override
	public String toString() {
		return this.id + "\t"+ this.name +"\t"+ this.phone;
	}

	@Override
	public int compareTo(Customer o) {
		return this.phone.compareToIgnoreCase(phone);
	}
	
	
	
}
