package test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import io.SerialableFactory;
import model.Customer;

public class TestProject {

	public static ArrayList<Customer> dsKH = new ArrayList<Customer>();

	public static void menu() {
		System.out.println("1. Input customer");
		System.out.println("2. Output customer");
		System.out.println("3. Search customer");
		System.out.println("4. Sort customer");
		System.out.println("5. Save customer");
		System.out.println("6. Read customer");
		System.out.println("7. Statistics customer");
		System.out.println("8. Exit");
		int select = 0;
		System.out.println("please select function from 1 to 8\n");
		select = new Scanner(System.in).nextInt();
		switch (select) {
		case 1:
			handlingInput();
			break;
		case 2:
			handlingOutput();
			break;
		case 3:
			handlingSearch();
			break;
		case 4:
			handlingSort();
			break;
		case 5:
			handlingSaveFile();
			break;
		case 6:
			handlingReadFile();
			break;
		case 7:
			handlingStatistics();
			break;
		case 8:
			handlingExit();
			break;
		default:
			break;
		}
	}

	private static void handlingExit() {
		// TODO Auto-generated method stub
		System.out.println("see you again");
		System.exit(0);
	}

	private static void handlingStatistics() {
		int n = 0;
		for (Customer kh:dsKH) {
			if(kh.getPhone().startsWith("090"));
			n++;
		}
		System.out.println(n + " customer have first number 090" );
	}

	private static void handlingReadFile() {
		dsKH = SerialableFactory.readfile("d:\\test.txt");
		System.out.println("read success");
		System.out.println("id\tname\tphone");
		for (Customer kh:dsKH) {
			System.out.println(kh);
		}
		
	}

	private static void handlingSaveFile() {
		boolean kt = SerialableFactory.saveFile(dsKH, "d:\\test.txt");
		if(kt == true){
			System.out.println("saved");
		}
		else{
			System.out.println("save fail");
		}
	}

	private static void handlingSort() {
		Collections.sort(dsKH);
		System.out.println("sorted phone");
		
	}

	private static void handlingSearch() {
		
		String phone = "090";
		
		System.out.println("customer have first number 090 ");
		System.out.println("id\tname\tphone");
		for (Customer kh: dsKH) {
			
			if (kh.getPhone().startsWith(phone)) {
				System.out.println(kh);
			}
			 	
			}
		System.out.println("========================");
		}
		

	private static void handlingOutput() {
		System.out.println("===================");
		System.out.println("id\tname\tphone");
		for (Customer kh:dsKH) {
			System.out.println(kh);
		}
		System.out.println("========================");
	}

	private static void handlingInput() {
		Customer kh = new Customer();
		System.out.println("please input id  \n");
		int id = new Scanner(System.in).nextInt();
		System.out.println("Please input name  \n");
		String name = new Scanner(System.in).nextLine();
		System.out.println("please input phone number \n");
		String phone = new Scanner(System.in).nextLine();		
		kh.setId(id);
		kh.setTen(name);
		kh.setPhone(phone);
		dsKH.add(kh);
	}

	public static void main(String[] args) {
		while(true){
			menu();
		}
	}
}
