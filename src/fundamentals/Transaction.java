package fundamentals;

import java.util.Arrays;
import java.util.Comparator;

import library.StdOut;

public class Transaction implements Comparable<Transaction> {
	private final String who ; //customer
	private final Date date; //date
	private final double amount; //account
	private final double withdrawn;// 
	
	
	
	//	Initializes a new transaction from the given arguments.
	
	/* * @param who the person involved in this transaction
     * @param  when the date of this transaction
     * @param  amount the amount of this transaction
     * @param withdrawn the withdrawn from amount*/
	

	public Transaction(String who, Date date, double amount, double withdrawn) {
		if (Double.isNaN(amount) || Double.isInfinite(amount))
			StdOut.println("Amount can be not a number or infinite");
		if (Double.isNaN(withdrawn) || Double.isInfinite(withdrawn))
			StdOut.println("Amount can be not a number or infinite");
		if (withdrawn >  amount) StdOut.println(who + " Amount don't enough to withdrawn");
		this.who = who;
		this.date = date;
		this.amount = amount;
		this.withdrawn = withdrawn;
		
	}
	
//	 Initializes a new transaction by parsing a string of the form NAME DATE AMOUNT.
	public Transaction(String transection) {
		String[] a = transection.split("\\s+");
		who = a[0];
		date = new Date(a[1]);
		amount = Double.parseDouble(a[2]);
		withdrawn = Double.parseDouble(a[3]);
		if (withdrawn > amount) StdOut.println(who + " Amount don't enough to withdrawn");
		if(Double.isNaN(amount) || Double.isInfinite(amount))
			StdOut.println("Amount can be not a number or infinite");
		if(Double.isNaN(withdrawn) || Double.isInfinite(withdrawn))
			StdOut.println("Amount can be not a number or infinite");
		
 
	}
	
	
//	Returns the name of the customer involved in this transaction.
//	Returns a string representation of this transaction.
	
	
	public String who(){
		return who;
	}
	
//	Returns the date of this transaction.

	public Date date(){
		return date;
	}
	
//	Returns the amount of this transaction.

	public double amount(){
		return amount;
	}
	
	public double withdrawn(){
		return withdrawn;
	}
	
	@Override 
	public String toString(){
		return String.format("%-10s %10s %8.2f %8.2f", who , date, amount,withdrawn);
	}
	
//	Compares two transactions.
	public int compareTo(Transaction that){
		return Double.compare(this.amount, that.amount);
	}
//	Returns a hash code for this transaction.
	 /**
     * Compares this transaction to the specified object.
     */
    @Override
    public boolean equals(Object other) {
        if (other == this) return true;
        if (other == null) return false;
        if (other.getClass() != this.getClass()) return false;
        Transaction that = (Transaction) other;
        return (this.amount == that.amount) && (this.who.equals(that.who))
                                            && (this.date.equals(that.date))
                                            && (this.withdrawn == that.withdrawn);
    }


    /**
     * Returns a hash code for this transaction.
     */
    public int hashCode() {
        int hash = 1;
        hash = 31*hash + who.hashCode();
        hash = 31*hash + date.hashCode();
        hash = 31*hash + ((Double) amount).hashCode();
        hash = 31*hash + ((Double) withdrawn).hashCode();
        return hash;
        // return Objects.hash(who, when, amount, withdrawn);
    }
	


	
	//	Compares two transactions by customer name.
	public static class WhoOrder implements Comparator<Transaction>{
		@Override
		public int compare(Transaction v, Transaction w){
			return v.who.compareTo(w.who);
		}
	}
	
	//	Compares two transactions by date.
	public static class DateOrder implements Comparator<Transaction> {
		@Override
		public int compare(Transaction v, Transaction w){
			return v.date.compareTo(w.date);
		}
	}
//	Compares two transactions by amount.
	 public static class HowMuchOrder implements Comparator<Transaction>{
		 @Override
		 public int compare(Transaction v, Transaction w) {
			 return Double.compare(v.amount, w.amount);
		 }
	 }
//		Compares two transactions by withdrawn. 
	 public static class WithdrawnOrder implements Comparator<Transaction>{
		 @Override
		 public int compare(Transaction v, Transaction w){
			 return Double.compare(v.withdrawn, w.withdrawn);
		 }
	 }
	 
	public static void main(String[] args){
		Transaction[] a = new Transaction[4];
		a[0] = new Transaction("Turing   6/17/1990  212.3  150");
		a[1] = new Transaction("Tarjan   3/26/2002 4121.85 300");
		a[2] = new Transaction("Knuth    6/14/1999  288.34 100");
        a[3] = new Transaction("Dijkstra 8/22/2007 2678.40 600");
        
        StdOut.println();
        StdOut.println("Unsorted");
        for(int i =0; i< a.length; i++){
        	StdOut.println(a[i]);
        }
        StdOut.println();
        StdOut.println("Sort by name");
        Arrays.sort(a, new Transaction.WhoOrder());
        for(int i =0; i< a.length; i++){
        	 StdOut.println(a[i]);
        }
        StdOut.println();
        StdOut.println("Sort by date");
        
        Arrays.sort(a, new Transaction.DateOrder());
		for (int i = 0; i < a.length; i++) {
			StdOut.println(a[i]);
		}
		StdOut.println();
		StdOut.println("Sort by amount");
		Arrays.sort(a, new Transaction.HowMuchOrder());
		for (int i = 0; i < a.length; i++) {
			StdOut.println(a[i]);
		}
		StdOut.println();
		StdOut.println("Sort by withdrawn");
		Arrays.sort(a, new Transaction.WithdrawnOrder());
		for (int i = 0; i < a.length; i++) {
			StdOut.println(a[i]);
		}
		StdOut.println();
		
	}
}
