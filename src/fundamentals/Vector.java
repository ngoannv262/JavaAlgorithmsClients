package fundamentals;

import library.StdOut;

public class Vector {
	private int d; // dimension of the vector
	private double[] data; // array of vector's components

	/*
	 * Initializes a d-dimensional zero vector. d the dimension of the vector
	 */
	public Vector(int d) {
		this.d = d;
		data = new double[d];
	}

	/*
	 * Initializes a vector from either an array or a vararg list. The vararg
	 * syntax supports a constructor that takes a variable number of arugments
	 * such as Vector x = new Vector(1.0, 2.0, 3.0, 4.0).
	 */
	public Vector(double[] vector) {
		d = vector.length;
		// defensive copy so that client can't alter our copy of data
		data = new double[d];
		for (int i = 0; i < d; i++)
			data[i] = vector[i];
	}

	// Returns the dimension of this vector
	public int dimension() {
		return d;
	}

	// Returns the do product of this vector with the specified vector.
	public double dot(Vector vector) {
		// compare length of tow string if not equal throw ex
		if (this.d != vector.d)
			throw new IllegalArgumentException("Dimensions don't agree");
		double sum = 0.0;
		for (int i = 0; i < d; i++)
			sum = sum + (this.data[i] * vector.data[i]);
		return sum;
	}

	// Returns the magnitude of this vector
	public double magnitude() {
		return Math.sqrt(this.dot(this));
	}

	// Returns the Euclidean distance between this vector and the specified
	// vector.

	public double distanceTo(Vector that) {
		// compare length of tow string if not equal throw ex
		if (this.d != that.d)
			throw new IllegalArgumentException("Dimensions don't agree");
		return this.minus(that).magnitude();
	}

	// Returns the sum of this vector and the specified vector.
	public Vector plus(Vector vector) {

		if (this.d != vector.d)
			throw new IllegalArgumentException("Dimensions don't agree");
		Vector c = new Vector(d);
		for (int i = 0; i < d; i++)
			c.data[i] = this.data[i] + vector.data[i];
		return c;
	}

	// Returns the difference between this vector and the specified vector.
	public Vector minus(Vector vector) {

		if (this.d != vector.d)
			throw new IllegalArgumentException("Dimensions don't agree");
		Vector c = new Vector(d);
		for (int i = 0; i < d; i++)
			c.data[i] = this.data[i] - vector.data[i];
		return c;
	}

	// Returns the ith cartesian coordinate
	public double cartesian(int i) {
		return data[i];
	}

	// Returns the scalar-vector product of this vector and the specified scalar
	public Vector times(double alpha) {
		Vector c = new Vector(d);
		for (int i = 0; i < d; i++)
			c.data[i] = alpha * data[i];
		return c;
	}

	// Returns a unit vector in the direction of this vector
	public Vector direction() {
		if (this.magnitude() == 0.0)
			throw new ArithmeticException("Zero-vector has no direction");
		return this.times(1.0 / this.magnitude());
	}

	// Returns a string representation of this vector.
	public String toString() {
		StringBuffer s = new StringBuffer();
		for (int i = 0; i < d; i++)
			s.append(data[i] + " ");
		return s.toString();
	}

	public static void main(String[] args) {
		double[] xdata = { 1.0, 2.0, 3.0, 4.0 };
		double[] ydata = { 5.0, 2.0, 4.0, 1.0 };

		double[] narray = { 2, 3, 4, 5, 6, 7, 8, 9 };

		Vector x = new Vector(xdata);
		Vector y = new Vector(ydata);
		Vector n = new Vector(narray);

		StdOut.println("Vector x =" + x);
		StdOut.println("Vector y =" + y);
		StdOut.println("Vector n =" + n);

		// Vector z = x.plus(n); // length of vector x and vector n are not
		// equals -- > at here throw ex
		// StdOut.println(" z =" + z);

		Vector m = x.plus(y);
		StdOut.println("x[i] + y[i] =" + m);

		n = n.times(2);
		StdOut.println("2* n =" + n);

		// sqrt (sum(x[i] * x [i]))
		StdOut.println("  |x|      = " + x.magnitude());

		StdOut.println("  |y|      = " + y.cartesian(3));
		// sum( x[i] * y[i])
		StdOut.println(" <x, y>    = " + x.dot(y));
		// distance = sqrt(sum ((y[i]-x[i])*(y[i]-x[i]))
		StdOut.println("disttance(x, y) = " + x.distanceTo(y));

		StdOut.println("distance(x,y)     = " + x.minus(y).magnitude());
		StdOut.println("minus(x,y)     = " + x.minus(y));
		StdOut.println("dir(x)     = " + x.direction());

	}
}
