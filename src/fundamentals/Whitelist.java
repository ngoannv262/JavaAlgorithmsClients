package fundamentals;

import library.In;
import library.StdIn;
import library.StdOut;

public class Whitelist {
	
    private Whitelist() { }
   

   

    /**
     * Reads in a sequence of integers from the whitelist file, specified as
     * a command-line argument. Reads in integers from standard input and
     * prints to standard output those integers that are not in the file.
     */
    public static void main(String[] args) {
    	 In in = new In("http://algs4.cs.princeton.edu/11model/tinyW.txt");
    	 // add all numbers in a array to white array  
         int[] white = in.readAllInts();
         for (int i = 0; i < white.length; i++) {
			System.out.print(white[i] + " ");
		}
         System.out.println(" ");
        // check dup number in white array
         
        StaticSETofInts set = new StaticSETofInts(white);
        // input number and Read key, print if key is not in white list. 
        StdOut.println("input number to check: ");
        while (!StdIn.isEmpty()) {       	
            int key = StdIn.readInt();
            //  compare number inputed with number in array
            if (!set.contains(key))
            	StdOut.println(key + " : this number is not have in array ");
                
        }
    }
}
