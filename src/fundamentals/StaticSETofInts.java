
package fundamentals;

import java.util.Arrays;



public class StaticSETofInts{
	 private int[] a;
//	 @param keys the array of integers
//   @throws IllegalArgumentException if the array contains duplicate integers
	 
	 public StaticSETofInts(int[] keys){
		 //defensive copy
		 a = new int [keys.length];
		 for(int i = 0; i< keys.length; i++){
			 a[i] = keys[i];
		 }
	 
		//sort  the intergers
		 Arrays.sort(a);
		 // check duplicates number in array
		 for (int i =1; i<a.length; i++)
			 if(a[i] == a[i-1])
				 throw new IllegalArgumentException("Argument arrays contains duplicate keys.");
			 
	 }
	 // check keys in this set of intergers
	 public boolean contains(int key){
		 return rank(key) != -1;
	 }
	 
	 /*Returns either the index of the search key in the sorted array
    	(if the key is in the set) or -1 (if the key is not in the set).
    */
	 
	 public int rank(int  key){
		 int lo = 0;
		 int hi = a.length - 1;
		 while (lo<= hi){
			// Key is in a[lo..hi] or not in a
			 int mid = lo +(hi - lo)/2;
			 if (key < a[mid] ) hi = mid - 1;
			 else if (key < a[mid]){
				 lo = mid + 1;
			 }
			 	else {
					return mid;
				}
		 }
		return -1;
	 }
}