
/*
 *  A generic bag or multiset, implemented using a singly-linked list.
 *
 *  % more tobe.txt 
 *  to be or not to - be - - that - - - is
 *
 *  % java Bag < tobe.txt
 *  size of bag = 14
 *  is
 *  -
 *  -
 *  -
 *  that
 *  -
 *  -
 *  be
 *  -
 *  to
 *  not
 *  or
 *  be
 *  to
 *  */
package fundamentals;

import java.util.Iterator;

import java.util.NoSuchElementException;

import library.StandardInput;

import library.StdOut;

public class Bag<Item> implements Iterable<Item> {
	private Node<Item> first; // beginning of bag
	private int n; // number of elements in bag

	// helper linked list class
	private static class Node<Item> {
		private Item item;
		private Node<Item> next;
	}

	/**
	 * Initializes an empty bag.
	 */
	public Bag() {
		first = null;
		n = 0;
	}

	/**
	 * Returns true if this bag is empty.
	 */
	public boolean isEmpty() {
		return first == null;
	}

	/**
	 * Returns the number of items in this bag.
	 */
	public int size() {
		return n;
	}

	/**
	 * Adds the item to this bag.
	 */
	public void add(Item item) {
		Node<Item> oldfirst = first;
		first = new Node<Item>();
		first.item = item;
		first.next = oldfirst;
		n++;
	}

	/**
	 * Returns an iterator that iterates over the items in this bag in arbitrary
	 * order.
	 */
	public Iterator<Item> iterator() {
		return new ListIterator<Item>(first);
	}

	// an iterator, doesn't implement remove() since it's optional
	private class ListIterator<Item> implements Iterator<Item> {
		private Node<Item> current;

		public ListIterator(Node<Item> first) {
			current = first;
		}

		public boolean hasNext() {
			return current != null;
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}

		public Item next() {
			if (!hasNext())
				throw new NoSuchElementException();
			Item item = current.item;
			current = current.next;
			return item;
		}
	}

	/**
	 * Unit tests the <tt>Bag</tt> data type.
	 */
	public static void main(String[] args) {
		Bag<String> bag = new Bag<String>();
		String[] item = StandardInput.input(null).toString().split(" ");
		for (int i = 0; i < item.length; i++) {
			bag.add(item[i]);
		}
		StdOut.println("size of bag = " + bag.size());
		for (String s : bag) {
			StdOut.println(s);
		}

	}
}
