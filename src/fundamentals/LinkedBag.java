package fundamentals;
/*
 * A generic bag or multiset, implemented using a singly-linked list.
*
*  % more tobe.txt 
*  to be or not to - be - - that - - - is
*
*  % java Bag < tobe.txt
*  size of bag = 14
*  is
*  -
*  -
*  -
*  that
*  -
*  -
*  be
*  -
*  to
*  not
*  or
*  be
*  to
*  */

import java.util.Iterator;
import java.util.NoSuchElementException;
import library.StandardInput;


public class LinkedBag<Item> implements Iterable<Item> {
	private First first;
	private int n;
//	helper linked list class
	private class First {
		private Item item;
		private First next;
	}
//	Initializes an empty bag.
	public LinkedBag() {
		first = null;
		n = 0;
	}
//	Is this bag empty?
	public boolean isEmpty() {
		return first == null;
	}
//	Returns the size of this bag.
	public int size() {
		return n;
	}
//	Adds the item to this bag.
	public void add(Item item) {
		First oldfirst = first;
		first = new First();
		first.item = item;
		first.next = oldfirst;
		n++;
	}
//	Returns an iterator that iterates over the items in the bag.
	public Iterator<Item> iterator() {
		return new ListIterator();
	}
//	an iterator, doesn't implement remove() since it's optional
	public class ListIterator implements Iterator<Item> {
		private First current = first;

		public boolean hasNext() {
			return current != null;
		}

		public void remove() {
			throw new UnsupportedOperationException();
		}

		public Item next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}
			Item item = current.item;
			current = current.next;
			return item;
		}
	}
//	Unit tests the LinkedBag data type
	public static void main(String[] args){
		LinkedBag<String> bag = new LinkedBag<String>();
		String[] item = StandardInput.input(null).toString().split(" ");
		for (int i = 0; i < item.length; i++) {
			bag.add(item[i]);
		}
		System.out.println("size of bag " + bag.size());
		for (String s : bag) {
			System.out.println(s);
		}
	}
}
