package fundamentals;

import library.In;





public class Knuth {
	
	
	private Knuth(){}
	
	
	public static void shuffle(String[] a){
		int N = a.length;
		for(int i = 0; i < N; i++){
			// choose index uniformly in [i, N-1]
			int r = i + (int)(Math.random() * (N - i));
			String swap = a[r];
			a[r] = a[i];
			a[i] = swap;
		}
	}
	
	public static void main(String[] args){
		//int[] a = { 100, 200, 10, 20, 30, 1, 2, 3 };
		In in = new In("cards.txt");
		String[] a = in.readAllStrings();
		
		// Shuffle integer array.
		Knuth.shuffle(a);
		// Display elements in array.
		for (int i = 0; i < a.length; i++) {
			System.out.println(a[i]);
		}
	}
	
}
