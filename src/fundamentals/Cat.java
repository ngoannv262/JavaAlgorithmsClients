
/******************************************************************************
 *  Compilation:  javac Cat.java
 *  Execution:    java Cat input0.txt input1.txt ... output.txt
 *  Dependencies: In.java Out.java
 *
 *  Reads in text files specified as the first command-line 
 *  arguments, concatenates them, and writes the result to
 *  filename specified as the last command-line arguments.
 *
 *  % more in1.txt
 *  This is
 *
 *  % more in2.txt 
 *  a tiny
 *  test.
 * 
 *  % java Cat in1.txt in2.txt out.txt
 *
 *  % more out.txt
 *  This is
 *  a tiny
 *  test.
 *
 ******************************************************************************/

package fundamentals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;



public class Cat{

	public static String getDictionaryFilePath(String filename) throws Exception {
		String dictionaryFolderPath = null;
		File configFolder = new File(filename);
		try {
			dictionaryFolderPath = configFolder.getAbsolutePath();

		} catch (Exception e) {
			throw new Exception(e);
		}
		return dictionaryFolderPath;
	}

	/**
	 * Reads in a sequence of text files specified arguments, concatenates them,
	 * and writes the results to the other file
	 * 
	 */
	public static void main(String args[]) throws Exception {
		String[] filename = { "in1.txt", "in2.txt" };
		File file = new File("out.txt");
		FileWriter output = new FileWriter(file);
		try {
			for (int i = 0; i < filename.length; i++) {
				/*
				 * Read all file in a sequence of text file and arguments,
				 * concatenates them, and writes the results to the other file
				 */
				BufferedReader objBufferedReader = new BufferedReader(
						new FileReader(getDictionaryFilePath(filename[i])));
				String line;
				while ((line = objBufferedReader.readLine()) != null) {
					//line = line.replace(" ", " ");
					output.write(line);;
				}
				objBufferedReader.close();
			}
			output.close();

			System.out.println("done");
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

}
