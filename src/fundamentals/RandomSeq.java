package fundamentals;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import library.StdOut;
import library.StdRandom;


public class RandomSeq {
	
	
	/*private Random random = new Random();
	
	public String getRandomList(List<String> list) {		
	    int index = random.nextInt(list.size());
	    return list.get(index);
	    
	};
	public static void main(String[] args) {

		List<String> list = new ArrayList<String>();
		list.add("12");
		list.add("23");
		list.add("34");
		list.add("41");
		list.add("54");

		RandomSeq obj = new RandomSeq();
		for(int i = 0; i < 2; i++){
			System.out.println(obj.getRandomList(list));
		}
		
	}*/
	
	
public static void main(String[] args) {
		
		
		String[] a ={"5","8","10"};
		
		String[] b = {"12"};
		 // command-line arguments
        int N = Integer.parseInt(a[2]);
        int N1 = Integer.parseInt(b[0]);
        // for backward compatibility with Intro to Programming in Java version of RandomSeq
        if (a.length == 1) {
            // generate and print N numbers between 0.0 and 1.0
            for (int i = 0; i < N; i++) {
                double x = StdRandom.uniform();
                StdOut.println(x);
            }
        }

        else if (a.length == 3) {
            double lo = Double.parseDouble(a[0]);
            double hi = Double.parseDouble(a[2]);

            // generate and print N numbers between lo and hi
            for (int i = 0; i < N; i++) {
                double x = StdRandom.uniform(lo, hi);
                StdOut.printf("%.2f\n", x);
            }
        }

        else {
            throw new IllegalArgumentException("Invalid number of arguments");
        }
    }
}