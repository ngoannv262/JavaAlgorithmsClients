package fundamentals;

/*  Data files:   http://algs4.cs.princeton.edu/11model/tinyW.txt
 *                http://algs4.cs.princeton.edu/11model/tinyT.txt
 *                http://algs4.cs.princeton.edu/11model/largeW.txt
 *                http://algs4.cs.princeton.edu/11model/largeT.txt
*/

import java.util.Arrays;
import java.util.Scanner;

import library.In;


public class BinarySearch
{
	
		
   public static void main(String args[])
   {
      int  key, lo, hi, mid;
      
      // read number from file
      In in = new In("http://algs4.cs.princeton.edu/11model/tinyW.txt");
      int[] whitelist = in.readAllInts();
      
    // out array from file
      System.out.println("out array from file : ");
      for (int i = 0; i < whitelist.length; i++) {
          System.out.print(" "+whitelist[i]);
          
      }
     // sort the array
      Arrays.sort(whitelist);
      //print array again
      System.out.print("sort the array :%.2f\n ");
      for(int i=0;i<whitelist.length;i++) 
       	  System.out.println(whitelist[i]); 
      
     //input number to search
    //To capture user input
      Scanner input = new Scanner(System.in);
      System.out.println("Enter the search key:");
      key = input.nextInt();
      lo = 0;
      hi = whitelist.length - 1;
      mid = (lo + hi)/2;
     
      while( lo <= hi )
      {
         if ( whitelist[mid] < key ) lo = mid + 1;
         else if ( whitelist[mid] == key ) key = input.nextInt();
         else hi = mid - 1;	         
         mid = (lo + hi)/2;
      }
      if ( lo > hi ) System.out.println(key + " is not in array.\n");
      if(input != null) input.close();	    	
   }


}