package fundamentals;

import library.StdOut;

public class Average {
	// this class should not be instantiated
    private Average() { }
    
    /**
     * Reads in a sequence of real numbers from standard input and prints
     * out their average to standard output.
     */
    public static void main(String[] args) { 
    	int[] numbers = new int[]{10,20,15,25,16,60,100};
    	StdOut.println("Print array");
    	for (int i = 0; i < numbers.length; i++) {
    		System.out.println(numbers[i]);
		}   	
        int count = 0;       // number input values
        double sum = 0.0;    // sum of input values

        //  compute statistics
        for(int i=0; i < numbers.length ; i++){
            sum = sum + numbers[i];
        	count++;
        }
        // compute the average
        double average = sum / count;

        // print results
        System.out.println("Average is " + average);
    }
}