package fundamentals;

public class Accumulator {
	private int n = 0;			// number of data values
	private double sum = 0.0; 	// sample variance * (n-1)
	private double mu = 0.0;	// sample mean

	public Accumulator() {
	}
//	Adds the specified data value to the accumulator.
	public void addValue(double x) {
		n++;
		double delta = x - mu;
		mu += delta / n;
		sum += (double) (n - 1) / n * delta * delta;
	}
//	Returns the mean of the data values.
	public double mean() {
		return mu;
	}
//	Returns the sample variance of the data values.
	public double var() {
		return sum / (n - 1);
	}
//	Returns the sample standard deviation of the data values.
	public double stddev() {
		return Math.sqrt(this.var());
	}
//	Returns the total of data values.
	public int count() {
		return n;
	}

	/*Reads in a stream of real number from file;
    * adds them to the accumulator; and prints the mean,
    * sample standard deviation, and sample variance to standard
    * output.*/
	
	public static void main(String[] args) {
		Accumulator stats = new Accumulator();
		double[] x = {2.4 , 4.5, 3.5};
		for (int i = 0; i < x.length; i++) {
			stats.addValue(x[i]);
		}
		System.out.printf("n      = %d\n", stats.count());
		System.out.printf("mean   = %.5f\n", stats.mean());
		System.out.printf("stddev = %.5f\n", stats.stddev());
		System.out.printf("var    = %.5f\n", stats.var());
	}
}
