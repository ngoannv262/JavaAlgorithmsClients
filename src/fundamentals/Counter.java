package fundamentals;

import library.StdRandom;

/* A mutable data type for an integer counter.
*
*  The test clients create n counters and performs trials increment
*  operations on random counters.*/

public class Counter implements Comparable<Counter> {

	private final String name; // couter name
	private int count = 0; // current value
	/* @param id the of the counter */

	public Counter(String id) {
		name = id;
	}

	/* increment the counter */
	public void increment() {
		count++;
	}

	/* Returns the current value of this counter */
	public int current() {
		return count;
	}

	/* Returns a string representation of this counter. */
	public String toString(){
		return count + " " + name;
	}
	
	/*Compares this counter to the specified counter.
	 * @param  that the other counter
	 * @return <tt>0</tt> if the value of this counter equals
     *         the value of that counter; a negative integer if
     *         the value of this counter is less than the value of
     *         that counter; and a positive integer if the value
     *         of this counter is greater than the value of that
     *         counter
	 * */
		
	@Override
	 public int compareTo(Counter counter){
		if (this.count <counter.count) return -1;
		else if (this.count > counter.count) return +1;
		else return 0;
			
		}

	public static void main(String[] args) {
		String[] a = {"1","2","3","4"};
		String[] b = {"5","6","7","8"};
		int n = Integer.parseInt(a[2]);
		int trials = Integer.parseInt(b[0]);
		
		// create n counters
		Counter[] hits = new Counter[n];
		for(int i = 0; i<n; i++){
			hits[i] = new Counter("counter" + i);
		}
		// incremet trials counter at random
		for(int t=0; t< trials; t++){
			hits[StdRandom.uniform(n)].increment();
		}
		//print results
		for (int l = 0; l < n; l++) {
            System.out.println(hits[l]);
        }
		
	}

}
