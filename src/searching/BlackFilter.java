package searching;

import library.In;
import library.StdIn;
import library.StdOut;

/**
 * Data files: http://algs4.cs.princeton.edu/35applications/tinyTale.txt
 * http://algs4.cs.princeton.edu/35applications/list.txt
 *
 * Read in a blacklist of words from a file. Then read in a list of words from
 * standard input and print out all those words that are not in the first file.
 */
public class BlackFilter {
	// Do not instantiate.
	private BlackFilter() {
	}

	public static void main(String[] args) {
		Set<String> set = new Set<String>();
		// read in string and add to set
		In in = new In("http://algs4.cs.princeton.edu/35applications/tinyTale.txt");
		while (!in.isEmpty()) {
			String word = in.readString();
			set.add(word);
		}
		// read in string from standard input, printing out all exceptions
		while (!StdIn.isEmpty()) {
			String word = StdIn.readString();
			if(!set.contains(word))
				StdOut.println(word);
		}
	}
}
