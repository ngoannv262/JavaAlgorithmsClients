package sorting;



import library.StandardInput;

public class Quick3way {
	private Quick3way() {
	};

	/**
	 * Rearranges the array in ascending order, using the natural order.
	 * 
	 * @param a the array to be sorted
	 */
	public static void sortarray(Comparable[] a) {
		sort(a, 0, a.length - 1);
	}

	// is v < w ?
	private static boolean less(Comparable v, Comparable w) {
		return v.compareTo(w) < 0;
	}

	// exchange a[i] and a[j]
	private static void exch(Object[] a, int i, int j) {
		Object swap = a[i];
		a[i] = a[j];
		a[j] = swap;
	}

	// quicksort the subarray a[lo .. hi] using 3-way partitioning
	public static void sort(Comparable[] a, int lo, int hi) {
		if (hi <= lo)
			return;
		int lt = lo, gt = hi;
		Comparable v = a[lo];
		int i = lo;
		while (i <= gt) {
			int cmp = a[i].compareTo(v);
			if (cmp < 0)exch(a, lt++, i++);
			else if (cmp > 0)exch(a, i, gt--);
			else i++;
			// a[lo..lt-1] < v = a[lt..gt] < a[gt+1..hi].
			sort(a, lo, lt - 1);
			sort(a, gt + 1, hi);
		}
	}
	
	// print array to standard output
		private static void show(Comparable[] a) {
			for (int i = 0; i < a.length; i++) {
				System.out.println(a[i]);
			}
		}
		/**
	     * Reads in a sequence of strings from standard input; 3-way
	     * quicksorts them; and prints them to standard output in ascending order. 
	     */
	public static void main(String[] args ) {		
		String[] a = StandardInput.input(null).toString().split(" ");
		Quick3way.sortarray(a);;
		show(a);
	}
}
