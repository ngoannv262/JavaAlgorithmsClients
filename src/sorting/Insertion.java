
/*
 *  Sorts a sequence of strings from file using insertion sort.
 *
 */

package sorting;

import java.util.Comparator;

import library.In;

import library.StdOut;




public class Insertion {

    // This class should not be instantiated.
    private Insertion() { }

    /**
     * Rearranges the array in ascending order, using the natural order.
     * @param a the array to be sorted
     */
    public static void sort(Comparable[] a) {
        int n = a.length;
        for (int i = 0; i < n; i++) {
            for (int j = i; j > 0 && less(a[j], a[j-1]); j--) {
                exch(a, j, j-1);
            }
           
        }
       
    }

    /**
     * Rearranges the subarray a[lo..hi] in ascending order, using the natural order.
     * @param a the array to be sorted
     * @param lo left endpoint
     * @param hi right endpoint
     */
    public static void sort(Comparable[] a, int lo, int hi) {
        for (int i = lo; i <= hi; i++) {
            for (int j = i; j > lo && less(a[j], a[j-1]); j--) {
                exch(a, j, j-1);
            }
        }
       
    }

    /**
     * Rearranges the array in ascending order, using a comparator.
     * @param a the array
     * @param comparator the comparator specifying the order
     */
    public static void sort(Object[] a, Comparator comparator) {
        int n = a.length;
        for (int i = 0; i < n; i++) {
            for (int j = i; j > 0 && less(a[j], a[j-1], comparator); j--) {
                exch(a, j, j-1);
            }
           
        }
       
    }

    /**
     * Rearranges the subarray a[lo..hi] in ascending order, using a comparator.
    
     */
    public static void sort(Object[] a, int lo, int hi, Comparator comparator) {
        for (int i = lo; i <= hi; i++) {
            for (int j = i; j > lo && less(a[j], a[j-1], comparator); j--) {
                exch(a, j, j-1);
            }
        }
       
    }


    // return a permutation that gives the elements in a[] in ascending order
   
    public static int[] indexSort(Comparable[] a) {
        int n = a.length;
        int[] index = new int[n];
        for (int i = 0; i < n; i++)
            index[i] = i;

        for (int i = 0; i < n; i++)
            for (int j = i; j > 0 && less(a[index[j]], a[index[j-1]]); j--)
                exch(index, j, j-1);

        return index;
    }

   /*
    *  Helper sorting functions.
    **/
    
    // is v < w ?
    private static boolean less(Comparable v, Comparable w) {
        return v.compareTo(w) < 0;
    }

    // is v < w ?
    private static boolean less(Object v, Object w, Comparator comparator) {
        return comparator.compare(v, w) < 0;
    }
        
    // exchange a[i] and a[j]
    private static void exch(Object[] a, int i, int j) {
        Object swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }

    // exchange a[i] and a[j]  (for indirect sort)
    private static void exch(int[] a, int i, int j) {
        int swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }

   // print array to standard output
    private static void show(Comparable[] a) {
        for (int i = 0; i < a.length; i++) {
            StdOut.print(a[i]+ " ");
        }
    }

    /**
     * Reads in a sequence of strings from file; insertion sorts them;
     * and prints them to standard output in ascending order.
     */
    public static void main(String[] args) {
    	
        In in = new In("tiny.txt");   	
        String[] a = in.readAllStrings();        
        // print a array
        StdOut.println("Print a array ");        
        for (int i = 0; i < a.length; i++) {
        	StdOut.print(a[i]+ " ");
		}
        StdOut.println();
       
        // insertion sorts a array
        Insertion.sort(a);        
        StdOut.println("Insertion sorts a array");
        show(a);
        StdOut.println();
        
        
        // insertion sorts b array
        In in1 = new In("words3.txt");
        String[] b = in1.readAllStrings();
        
        // print b array
        StdOut.println("Print b array ");
        for (int i = 0; i < b.length; i++) {
        	StdOut.print(b[i]+" ");
		}
        StdOut.println();
        StdOut.println("Insertion sorts b array");
        Insertion.sort(b);
        show(b);
        
    }
}

