package sorting;

/*
 * Sorts a sequence of strings from standard input using an optimized
*  version of insertion sort that uses half exchanges instead of 
*  full exchanges to reduce data movement..*/

import library.StdOut;
import library.*;

public class InsertionX {

	// This class should not be instantiated.
	private InsertionX() {
	}

	/**
	 * Rearranges the array in ascending order, using the natural order.
	 * 
	 * @param a
	 *            the array to be sorted
	 */
	public static void sort(Comparable[] a) {
		int n = a.length;
		// put smallest element in position to serve as sentinel
		int exchanges = 0;
		for (int i = n - 1; i > 0; i--) {
			if (less(a[i], a[i - 1])) {
				exch(a, i, i - 1);
				exchanges++;
			}
		}
		if (exchanges == 0)
			return;
		// insertion sort with half-exchanges
		for (int i = 2; i < n; i++) {
			Comparable v = a[i];
			int j = i;
			while (less(v, a[j - 1])) {
				a[j] = a[j - 1];
				j--;
			}
			a[j] = v;
		}
	}

	/*
	 * Helper sorting functions.
	 */

	private static boolean less(Comparable v, Comparable w) {
		return v.compareTo(w) < 0;
	}

	// exchange a[i] and a[j]
	private static void exch(Object[] a, int i, int j) {
		Object swap = a[i];
		a[i] = a[j];
		a[j] = swap;
	}

	// print array to standard output
	private static void show(Comparable[] a) {
		for (int i = 0; i < a.length; i++) {
			StdOut.println(a[i]);
		}
	}

	/**
	 * Reads in a sequence of strings from standard input; insertion sorts them;
	 * and prints them to standard output in ascending order.
	 */
	public static void main(String[] args) {
		String[] a = StandardInput.input(null).toString().split(" ");
		;
		InsertionX.sort(a);
		show(a);

	}

}
