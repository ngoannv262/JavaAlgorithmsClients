package sorting;

import library.StandardInput;

public class Quick{
	
	 private Quick() { }
   
	    // partition the subarray a[lo..hi] so that a[lo..j-1] <= a[j] <= a[j+1..hi]
	    // and return the index j.
	    private static int partition(Comparable[] a, int lo, int hi) {
	        int i = lo;
	        int j = hi + 1;
	        Comparable v = a[lo];
	        while (true) { 

	            // find item on lo to swap
	            while (less(a[++i], v))
	                if (i == hi) break;

	            // find item on hi to swap
	            while (less(v, a[--j]))
	                if (j == lo) break;      // redundant since a[lo] acts as sentinel

	            // check if pointers cross
	            if (i >= j) break;

	            exch(a, i, j);
	        }

	        // put partitioning item v at a[j]
	        exch(a, lo, j);

	        // now, a[lo .. j-1] <= a[j] <= a[j+1 .. hi]
	        return j;
	    }

	    /**
	     * Rearranges the array so that a[k] contains the kth smallest key;
	     * a[0] through a[k-1] are less than (or equal to) a[k]; and
	     * a[k+1] through a[N-1] are greater than (or equal to) a[k].
	     * @param a the array
	     * @param k find the kth smallest
	     */
	    public static Comparable select(Comparable[] a, int k) {
	        if (k < 0 || k >= a.length) {
	            throw new IndexOutOfBoundsException("Selected element out of bounds");
	        }
	       
	        int lo = 0, hi = a.length - 1;
	        while (hi > lo) {
	            int i = partition(a, lo, hi);
	            if      (i > k) hi = i - 1;
	            else if (i < k) lo = i + 1;
	            else return a[i];
	        }
	        return a[lo];
	    }



	   /*
	    *  Helper sorting functions.
	    */
	    
	    // is v < w ?
	    private static boolean less(Comparable v, Comparable w) {
	        return v.compareTo(w) < 0;
	    }
	        
	    // exchange a[i] and a[j]
	    private static void exch(Object[] a, int i, int j) {
	        Object swap = a[i];
	        a[i] = a[j];
	        a[j] = swap;
	    }

	    // print array to standard output
	    private static void show(Comparable[] a) {
	        for (int i = 0; i < a.length; i++) {
	            System.out.print(a[i]+ " ");
	        }
	    }

	    /**
	     * Reads in a sequence of strings from standard input; quicksorts them; 
	     * and prints them to standard output in ascending order. 
	     */
	    public static void main(String[] args) {
	    	String[] a = StandardInput.input(null).toString().split(" ");
	    	System.out.println("array you just input");
	        show(a);
	        System.out.println();
	        // display results again using select
	        System.out.println("Sort array a by using quicksort.");
	        for (int i = 0; i < a.length; i++) {
	            String ith = (String) Quick.select(a, i);
	            System.out.print(ith + " ");
	        }
	    }

}