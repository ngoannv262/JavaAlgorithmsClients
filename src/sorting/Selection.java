
// Sorts a sequence of strings from standard input using selection sort.
package sorting;

import java.util.Comparator;
import java.util.Scanner;

import library.StandardInput;
import library.StdOut;

public class Selection {

	// This class should not be instantiated.
	private Selection() {
	}

	// exchange a[i] and a[j]
	private static void exchange(Object[] a, int i, int j) {
		Object swap = a[i];
		a[i] = a[j];
		a[j] = swap;
	}

	/***************************************************************
	 * way 1
	 **************************************************************/
	/**
	 * Rearranges the array in ascending order, using the natural order.
	 * 
	 * @param a
	 *            is the array to be sorted
	 */
	public static void sort(Comparable[] a) {
		int n = a.length;
		for (int i = 0; i < n; i++) {
			int min = i;
			for (int j = i + 1; j < n; j++) {
				if (less(a[j], a[min]))
					min = j;
			}
			exchange(a, i, min);

		}

	}
	/*
	 * Helper sorting functions.
	 */

	// is v < w ?
	private static boolean less(Comparable v, Comparable w) {
		return v.compareTo(w) < 0;
	}

	// print array to standard output
	private static void show(Comparable[] a) {
		for (int i = 0; i < a.length; i++) {
			StdOut.println(a[i]);
		}
	}
	/***************************************************************
	 * Way 2
	 **************************************************************/

	/**
	 * Rearranges the array in ascending order, using a comparator.
	 * 
	 * @param a
	 *            the array
	 * @param comparator
	 *            the comparator specifying the order
	 */
	/*
	 * public static void sort(Object[] a, Comparator comparator) { int n =
	 * a.length; for (int i = 0; i < n; i++) { int min = i; for (int j = i + 1;
	 * j < n; j++) { if (less(comparator, a[j], a[min])) min = j; } exchange(a,
	 * i, min);
	 * 
	 * }
	 * 
	 * } private static void show2(Object[]a, Comparator comparator){ for (int i
	 * = 0; i < a.length; i++) { StdOut.println(a[i]); } }
	 * 
	 * 
	 * Helper sorting functions.
	 * 
	 * 
	 * // // is v < w ? private static boolean less(Comparator comparator,
	 * Object v, Object w) { return comparator.compare(v, w) < 0; }
	 */
	/**
	 * Reads in a sequence of strings from standard input; selection sorts them;
	 * and prints them to standard output in ascending order.
	 */
	public static void main(String[] args) {
		String[] b = StandardInput.input(null).toString().split(" ");
		Selection.sort(b);
		show(b);
	}
}
