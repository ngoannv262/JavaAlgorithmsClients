
/*
 *  Create an alphabet specified on the command line, read in a 
 *  sequence of characters over that alphabet (ignoring characters
 *  not in the alphabet), computes the frequency of occurrence of
 *  each character, and print out the results.
 * Data : http://algs4.cs.princeton.edu/50strings/abra.txt
 * 		: http://algs4.cs.princeton.edu/50strings/pi.txt
 *  %  java Count ABCDR < abra.txt 
 *  A 5
 *  B 2
 *  C 1
 *  D 1
 *  R 2
 *
 *  % java Count 0123456789 < pi.txt
 *  0 99959
 *  1 99757
 *  2 100026
 *  3 100230
 *  4 100230
 *  5 100359
 *  6 99548
 *  7 99800
 *  8 99985
 *  9 100106
 *
 */

package strings;

import java.util.HashMap;
import java.util.Map;

import library.In;

public class Count {

	// Do not instantiate.
	private Count() {
	}

	/**
	 * Reads in text from standard input; calculates the frequency of occurrence
	 * of each character over the alphabet specified as a commmand-line
	 * argument; and prints the frequencies to standard output.
	 */
	public static void main(String[] args) {
		In in = new In("http://algs4.cs.princeton.edu/50strings/pi.txt");
		String a = in.readString();
		int len = a.length();
		Map<Character, Integer> countChar = new HashMap<Character, Integer>(Math.min(len, 26));
		for (int i = 0; i < len; ++i) {
			char charAt = a.charAt(i);
			if (!countChar.containsKey(charAt)) {
				countChar.put(charAt, 1);
			} else
				countChar.put(charAt, countChar.get(charAt) + 1);

		}
		System.out.println(countChar);
	}
}
