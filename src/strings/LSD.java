package strings;

import library.StandardInput;

public class LSD {
	private LSD() {
	}

	/**  
     * Rearranges the array of W-character strings in ascending order.
     *
     * @param a the array to be sorted
     * @param w the number of characters per string
     */
	
	public static void sort(String[] a, int w){
		int n = a.length;
		// extend ASCII alphabet size
		int R = 256;
		String[] aux = new String[n];
		
		for (int d = w-1; d>= 0; d--){
			int[] count  = new int[R+1];
			// compute frequency counts
			for (int i = 0; i < n; i++)
				count[a[i].charAt(d) +1] ++;
			// compute cumulates
			for (int r = 0; r < R; r++) {
				count[r+1] += count[r];
			}
			//move data
			for (int i = 0; i < n; i++) {
				aux[count[a[i].charAt(d)]++] = a[i];
			}
			// copy back
			for (int i = 0; i < n; i++) {
				a[i] = aux[i];
			}
	}
}
	public static void main(String[] args){
		String[] a = StandardInput.input(null).toString().split(" ");
		int n= a.length;
		int w = a[0].length();
		//sort the string
		sort(a, w);
		// print results
		for (int i = 0; i < n; i++) {
			System.out.println(a[i]);
		}
		
	}
}